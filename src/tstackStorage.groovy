@Grapes([
    @Grab("io.ratpack:ratpack-groovy:1.5.4"),
    @Grab("org.slf4j:slf4j-simple:1.7.25")
])
import ratpack.groovy.Groovy

Groovy.ratpack {
  handlers {
    prefix("documents") {
      get { render("TODO get all") }
      post { render("TODO create") }
      get(":id") { render("TODO get by id: $pathTokens.id") }
      put(":id") { render("TODO save by id: $pathTokens.id") }
    }
  }
}
